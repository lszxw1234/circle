import unittest
from selenium import webdriver
from selenium.webdriver import FirefoxOptions


class PythonTest(unittest.TestCase):
    def setUp(self):
        opts = FirefoxOptions()
        opts.add_argument("--headless")
        self.driver = webdriver.Firefox(firefox_options=opts)
    def test_show_difference(self):
        driver = self.driver
        driver.get('http://localhost:8090/')
        elem_button = driver.find_element_by_id("addButton")
        elem_button.click()
        
        elem_text = driver.find_element_by_id("textOutput")
        assert "owentest" in elem_text.text
        

    def teatDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()