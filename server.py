#!/usr/bin/env python
# -*- coding: utf-8 -*-


from flask import Flask
from flask import Flask
from flask import render_template
from flask import request
from flask import redirect
from flask import url_for

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():

    return render_template('index.html')

if __name__ == '__main__':

    print(__name__)
    app.run()
